
import numpy as np
import math
import matplotlib.pyplot as plt

nmesh=100
dx=2/100
dy=.25/100
u=np.random.rand(100,100)
for i in range(100):
    u[i][0]=10000
v=np.random.rand(100,100)
alpha=0.01
rho=998.2
gamma=1.003/1000/rho
pstar=np.random.rand(100,100)
dt=0.001
re=10000
mu=1.003/1000
ls=1
lp=1

for iter in range(500):
    vstar=np.random.rand(100,100)
    ustar=np.random.rand(100,100)
    for i in range(100):
        ustar[i][0]=10000
    ptilda=np.random.rand(100,100)
    for i1 in range(2,nmesh):
        for j1 in range(2,nmesh):
            i=nmesh-i1
            j=nmesh-j1
            vstar[i][j]=(vstar[i][j+1]*(-(v[i][j+1]+v[i][j])/4*dx+gamma*dx/dy)+vstar[i][j-1]*((v[i][j]+v[i][j-1])/4*dy+gamma*dx/dy)+vstar[i+1][j]*(-(u[i][j+1]+u[i][j])/4*dy+gamma*dy/dx)+vstar[i-1][j]*((u[i-1][j+1]+u[i-1][j])/4*dy+gamma*dy/dx)+1/rho*(pstar[i][j]-pstar[i][j+1])+v[i][j]*dx*dy/dt)/(dx*dy/dt+(v[i][j+1]+v[i][j-1])/4*dx+(u[i][j+1]+u[i][j])/4*dx-(u[i-1][j+1]+u[i-1][j])/4*dx+2*gamma*dx/dy+2*gamma*dy/dx)
            ustar[i][j]=(ustar[i+1][j]*(-(u[i+1][j]+u[i][j])/4*dy+gamma*dy/dx)+ustar[i-1][j]*((u[i-1][j]+u[i][j])/4*dy+gamma*dy/dx)+ustar[i][j+1]*(-(v[i][j]+v[i+1][j])/4*dx+gamma*dx/dy)+ustar[i][j-1]*((v[i][j-1]+v[i+1][j-1])/4*dx+gamma*dx/dy)+1/rho*(pstar[i][j]-pstar[i][j+1])+u[i][j]*dx*dy/dt)/(dx*dy/dt+(u[i+1][j]-u[i-1][j])/4*dy+(v[i][j]+v[i+1][j])/4*dx-(v[i][j-1]+v[i+1][j-1])/4*dy+2*gamma*dx/dy+2*gamma*dy/dx)
            ptilda[i][j]=((dy/rho/ustar[i-1][j]*dy*ptilda[i-1][j]+dy/rho/ustar[i+1][j]*dy*ptilda[i+1][j]+dy/rho/vstar[i][j]*dy*ptilda[i][j+1]+dy/rho/vstar[i][j-1]*ptilda[i][j-1])-((ustar[i][j]-ustar[i-1][j])*dy+(vstar[i][j]-vstar[i][j-1])*dx))/(dy/ustar[i][j]/rho*dy+dy/ustar[i-1][j]/rho+dx/vstar[i][j]/rho+dx*dx/rho/vstar[i][j-1])
    pstar=pstar+alpha*(ptilda)
    ustar=ustar+alpha*(ustar-u)
    u=ustar
    v=vstar

U=re*mu/rho/(ls+lp)

eta=np.zeros(100)
fs=np.zeros(100)
for i in range(1,100):
    eta[i]=dy*math.sqrt(U/mu*rho/dx)
    dy+=.25/100
    dx+=2/100
    fs[i]=eta[i]/U

plt.plot(eta,fs)
